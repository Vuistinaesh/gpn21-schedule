import * as React from "react";
import {useMemo} from "react"
import {DateTime, Settings} from "luxon"
import './App.css';
import 'react-big-calendar/lib/css/react-big-calendar.css'
import {Calendar, luxonLocalizer, momentLocalizer, Views, Navigate} from "react-big-calendar";
import Schedule from "./schedule.json";
import moment from "moment";
import "moment-timezone";
import TimeGrid from "react-big-calendar/lib/TimeGrid"
import * as dates from 'date-arithmetic'

function FourDays({
                    date,
                    localizer,
                    max = localizer.endOf(new Date(), 'day'),
                    min = localizer.startOf(new Date(), 'day'),
                    scrollToTime = localizer.startOf(new Date(), 'day'),
                    ...props
                }) {
    const currRange = useMemo(
        () => FourDays.range(date, { localizer }),
        [date, localizer]
    )

    return (
        <TimeGrid
            date={date}
            eventOffset={15}
            localizer={localizer}
            max={max}
            min={min}
            range={currRange}
            scrollToTime={scrollToTime}
            {...props}
        />
    )
}

FourDays.range = (date, { localizer }) => {
    const start = date
    const end = dates.add(start, 3, 'day')

    let current = start
    const range = []

    while (localizer.lte(current, end, 'day')) {
        range.push(current)
        current = localizer.add(current, 1, 'day')
    }

    return range
}

FourDays.navigate = (date, action, { localizer }) => {
    switch (action) {
        case Navigate.PREVIOUS:
            return localizer.add(date, -4, 'day')

        case Navigate.NEXT:
            return localizer.add(date, 4, 'day')

        default:
            return date
    }
}

FourDays.title = (date) => {
    return `GPN: ${date.toLocaleDateString()}`
}

function App() {

    Settings.defaultZone = 'UTC+2'

    const localizer = luxonLocalizer(DateTime, {firstDayOfWeek: 1})

    const event = [];
    Schedule.schedule.conference.days.forEach(day => {
        Object.values(day.rooms).forEach(room => {
            console.log("room", room)
            room.forEach(session => {
                const start = new Date(session.date);
                const end = moment(Number(Date.parse(session.date))).add(session.duration.substring(0, 2), 'h').add(session.duration.substring(3, 5), 'm').toDate()
                event.push({
                    title: `${session.title} (${session.language} - ${session.room})`,
                    start: start,
                    end: end,
                    duration: session.duration
                })
            })
        })
    })

    console.log(event)
    console.log(event[1])

    const ColoredDateCellWrapper = ({children}) =>
        React.cloneElement(React.Children.only(children), {
            style: {
                backgroundColor: 'lightblue',
            },
        })

    const {components, defaultDate, min, views} = useMemo(
        () => ({
            components: {
                timeSlotWrapper: ColoredDateCellWrapper,
            },
            defaultDate: new Date("2023-06-08"),
            //views: Object.keys(Views).map((k) => Views[k]),
            views: {month: FourDays, work_week: true, week: true, agenda: true, day: true},
            min: new Date(1972, 0, 1, 8, 30)
        }),
        []
    )

    return (
        <div>
            <p>Test</p>
            <div style={{height: '6000px'}}>
                <Calendar
                    components={components}
                    defaultDate={defaultDate}
                    events={event}
                    localizer={localizer}
                    showMultiDayTimes
                    dayLayoutAlgorithm={"no-overlap"}
                    step={5}
                    views={views}
                    min={min}
                />
            </div>
        </div>
    );
}

export default App;
